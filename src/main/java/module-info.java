module com.mycompany.proyectoalfil {
    requires javafx.controls;
    requires javafx.fxml;

    opens Vista to javafx.fxml;
    exports Vista;
}
