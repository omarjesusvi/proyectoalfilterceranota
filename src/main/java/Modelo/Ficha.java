/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madarme
 */
public class Ficha {
    
    // alfil o peón
    private String nombreFicha;
    private int i;
    private int j;

    public Ficha() {
    }
    
    public Ficha(int i, int j, String nombreFicha){
        this.i = i;
        this.j = j;
        this.nombreFicha = nombreFicha;
    }

    public Ficha(String nombreFicha) {
        this.nombreFicha = nombreFicha;
    }

    public String getNombreFicha() {
        return nombreFicha;
    }

    public void setNombreFicha(String nombreFicha) {
        this.nombreFicha = nombreFicha;
    }
    
    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }  

    public void setI(int i) {
        this.i = i;
    }

    public void setJ(int j) {
        this.j = j;
    }

    @Override
    public String toString() {
        if(nombreFicha.equals("Alfil")){
            return "♗";
        }
        if(nombreFicha.equals("Peon")){
            return "♙";
        }
        return null;
    }
    
    
}
