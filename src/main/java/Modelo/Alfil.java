/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Alessandro Daniele
 */
public class Alfil extends Ficha {

    public Alfil(int i, int j, String nombreFicha) {
        super(i, j, "Alfil");
    }

    public void ataqueAlfil(Ficha myTablero[][]) {
        int i = this.getI();
        int j = this.getJ();
        while (i < 8 && j >= 0) {
            myTablero[i][j] = this;
            i++;
            j--;
        }
        i = this.getI();
        j = this.getJ();
        while (i < 8 && j < 8) {
            myTablero[i][j] = this;
            i++;
            j++;
        }

        i = this.getI();
        j = this.getJ();
        while (i >= 0 && j < 8) {
            myTablero[i][j] = this;
            i--;
            j++;
        }

        i = this.getI();
        j = this.getJ();
        while (i >= 0 && j >= 0) {
            myTablero[i][j] = this;
            i--;
            j--;
        }
    }
}
