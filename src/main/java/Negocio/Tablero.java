/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Alfil;
import Modelo.Ficha;

/**
 *
 * @author madar
 */
public class Tablero {
    private Ficha [][]myTablero;
    private Alfil alfil;
    private Ficha peon;
    
    public Tablero() {
    }
    
    /**
     * Constructor para iniciar el juego
     * @param i_alfil Posición de la fila para el alfil
     * @param j_alfil Posición de la columna para el alfil
     * @param i_peon  Posición de la fila para el peon
     * @param j_peon  Posición de la columna para el alfil
     * @param dirPeon true si el peón se mueve de arriba hacia abajo, o false en caso contrario
     */
     public Tablero(int i_alfil, int j_alfil, int i_peon,int j_peon, boolean dirPeon) {
         alfil = new Alfil(i_alfil, j_alfil, "Alfil");
         peon = new Ficha(i_peon, j_peon, "Peon");
         myTablero = new Ficha [8][8];
         
         //Posiciones en las cuales el alfil ataca
         alfil.ataqueAlfil(myTablero);
         
         //Posicion del peón
         myTablero[peon.getI()][peon.getJ()] = peon;
    }
    
    
    public String jugar()
    {
        
        return null;
    }
    
    public boolean findAtaqueAlfil(int movX, int movY){
        return myTablero[movX][movY] != null;
    }
    
    public int  jugarRecursivo(int mov){
        if(++mov >= 8 || findAtaqueAlfil(mov, peon.getJ())) {
            //Mover alfil
            return 0;
         }
        peon.setI(mov);
        myTablero[peon.getI()][peon.getJ()] = peon;
        System.out.println(this.toString());
        return jugarRecursivo(mov);
    }

    @Override
    public String toString() {
        String ans = "";
        for(int i=0; i < 8; i++){
            for(int j=0; j < 8; j++){
               // (myTablero[i][j] == null) ? ans += " x ": ans += " " +  myTablero[i][j] + " ";
                if(myTablero[i][j] == null){
                    ans += " x ";
                }else{
                     ans += " " +  myTablero[i][j] + " ";
                }
            }
            ans += "\n";
        }
        return ans;
    }
    
    
}
