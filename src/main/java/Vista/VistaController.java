package Vista;

/**
 * Sample Skeleton for 'Alfil.fxml' Controller Class
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class VistaController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="btn_imprimir"
    private Button btn_imprimir; // Value injected by FXMLLoader

    @FXML // fx:id="btn_jugar"
    private Button btn_jugar; // Value injected by FXMLLoader

    @FXML // fx:id="cbox_direccion"
    private ComboBox<?> cbox_direccion; // Value injected by FXMLLoader

    @FXML
    void imprimirPdf(ActionEvent event) {

    }

    @FXML
    void jugar(ActionEvent event) {

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert btn_imprimir != null : "fx:id=\"btn_imprimir\" was not injected: check your FXML file 'Alfil.fxml'.";
        assert btn_jugar != null : "fx:id=\"btn_jugar\" was not injected: check your FXML file 'Alfil.fxml'.";
        assert cbox_direccion != null : "fx:id=\"cbox_direccion\" was not injected: check your FXML file 'Alfil.fxml'.";

    }

}
