/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Negocio.Tablero;

/**
 *
 * @author Alessandro Daniele
 */
public class Main {
    public static void main(String[] args) {
        Tablero tablero= new Tablero(2, 1, 2, 6, true);
        tablero.jugarRecursivo(2);
    }
}
